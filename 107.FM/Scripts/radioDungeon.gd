extends Node3D
@onready var audio_stream_player_3d = $AudioStreamPlayer3D
@onready var jugador = $"../../Jugador"

var caso = 1
var textoCaso = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	

func _on_animation_player_animation_finished(anim_name):
	if anim_name == "AnimacionABlanco":
		if textoCaso == 12:
			global_position =  Vector3(-0.964, -13.689, -21.61)
			rotation_degrees = Vector3(0, 180, 0)
			jugador.rotation_degrees = Vector3(0, 0, 0)
			audio1()
		if textoCaso == 31:
			global_position =  Vector3(32.149, -13.718, 5.487)
			rotation_degrees = Vector3(0, -146.6, 0)
			jugador.rotation_degrees = Vector3(0, 0, 0)
			audio2()
		if textoCaso == 50:
			global_position =  Vector3(-16.614, -13.718, -20.24)
			rotation_degrees = Vector3(0, 176.9, 0)
			jugador.rotation_degrees = Vector3(0, 0, 0)
			audio3()
			
	

func audio1():
	var audioStream: AudioStream = preload("res://Assets/Audio/31242.mp3")
	$AudioStreamPlayer3D.set_stream(audioStream)
	$AudioStreamPlayer3D.set_volume_db(3.0)
	$AudioStreamPlayer3D.play()
func audio2():
	var audioStream: AudioStream = preload("res://Assets/Audio/4124.mp3")
	$AudioStreamPlayer3D.set_stream(audioStream)
	$AudioStreamPlayer3D.set_volume_db(3.0)
	$AudioStreamPlayer3D.play()
func audio3():
	var audioStream: AudioStream = preload("res://Assets/Audio/41242.mp3")
	$AudioStreamPlayer3D.set_stream(audioStream)
	$AudioStreamPlayer3D.set_volume_db(3.0)
	$AudioStreamPlayer3D.play()
	
	
func _on_static_body_3d_2_interacted(body):
	$AudioStreamPlayer3D.stop()


func _on_texto_container_hidden():
	textoCaso += 1 
