extends Label
@onready var canvas_fade = $"../../CanvasFade"
@onready var animation_player = $"../../CanvasFade/AnimationPlayer"

var velocidad = 45

# Called when the node enters the scene tree for the first time.
func _ready():
	$"../../CanvasFade/AnimationPlayer".play("AnimacionABlanco")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position.y -= velocidad * delta
