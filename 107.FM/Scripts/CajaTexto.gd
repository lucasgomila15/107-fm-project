extends CanvasLayer

const CHAR_READ_RATE = 0.05

@onready var textbox_container = $TextoContainer
@onready var start_symbol = $TextoContainer/MarginContainer/HBoxContainer/Comienzo
@onready var end_symbol = $TextoContainer/MarginContainer/HBoxContainer/Final
@onready var label = $TextoContainer/MarginContainer/HBoxContainer/Texto
@onready var jugador = $"../Jugador"

var activo = false
var caso = 1
var textoCaso = 0
var seguir = false
var interactuableRadio = true

enum State {
	READY,
	READING,
	FINISHED
}

var current_state = State.READY
var text_queue = []

func _ready():
	print("Starting state: State.READY")
	hide_textbox()
	
func _process(delta):
	var posicion = jugador.global_position
	var rangoPosicion1 = Vector3(3, 0, -7)
	
	match current_state:
		State.READY:
			if !text_queue.is_empty():
				display_text()
		State.READING:
			if Input.is_action_just_pressed("Interactuar"):
				label.visible_ratio = 1.0
				end_symbol.text = "v"
				change_state(State.FINISHED)
		State.FINISHED:
			if Input.is_action_just_pressed("Interactuar"):
				change_state(State.READY)
				hide_textbox()
	
	if seguir && !activo:
		if caso == 2:
			activo = true
			print("Starting state: State.READY")
			hide_textbox()
			label.add_theme_color_override("font_color", "c56231e5")
			queue_text("Buenas noches y bienvenidos a la radio 107FM, en esta noche tenemos muchas canciones que pasar y temas de los que hablar.")
			queue_text("No se si se enteraron sobre la demolicion del Edificio Sunshine, algo increible no? Por que un edificio tan importante puede ser demolido sin previo aviso?")
			queue_text("Esperemos que las 33 familias que vivian alli puedan encontrar algun hogar pronto.")
			queue_text("Pasando a otras informaciones que nos llegan, es sobre la banda Red Floyd que vuelve despues de muchisimo tiempo, algo increible no?")
			queue_text("Asi que mandandole un saludo a nuestro oyente Damian que nos escucha desde Wilde, escuchamos 31224 de Red Floyd.")
	
		if caso == 3:
			activo = true
			print("Starting state: State.READY")
			hide_textbox()
			label.add_theme_color_override("font_color", "ffffff")
			queue_text("Que dicen en la radio? El edificio Sunshine? No puede ser, vivia ahi con mi madre cuando era chico")
			queue_text("Pero no creo que esa informacion sobre el edificio sea cierta, mi madre me hubiera avisado si se mudaba o algo.")
			queue_text("No se por que me preocupo realmente, si no hablo con ella.")
			queue_text("Igualmente me encanta Red Floyd, me acuerdo que cuando vivia en el Sunshine, volvia a casa despues del colegio y me ponia a escuchar sus discos.")
			queue_text("Pero siendo sinceros no recuerdo bien por que me aleje de ella, tengo que volver a recordar?")
			
		if caso == 5:
			activo = true
			print("Starting state: State.READY")
			hide_textbox()
			queue_text("Claro, como me voy a poder olvidar?")
		
		if caso == 6 && posicion.distance_to(rangoPosicion1) < 1:
			activo = true
			print("Starting state: State.READY")
			hide_textbox()
			queue_text("Ya recuerdo. Habia sido antes del campamento, mis amigos me habian llamado para ir con ellos, pero ella no queria que fuera porque me iban a pegar la suciedad y locura, ironico no?")
			queue_text("Encima todavia tengo las marcas de quemaduras de cigarillos y de golpes cuando no hacia lo que queria.")
			queue_text("Por suerte esa noche me escape y me pude ir sin que se dieran cuenta, pero no recuerdo del todo.")
				
		if caso == 7:
			activo = true
			print("Starting state: State.READY")
			hide_textbox()
			label.add_theme_color_override("font_color", "c56231e5")
			queue_text("Seguimos en esta noche maravillosa donde nos preguntamos a veces, a donde vamos y donde queremos llegar?")
			queue_text("Que queremos? Y por que seguimos queriendo cosas que nunca van a llegar?")
			queue_text("Puede sonar pesimista, pero es la realidad. Por eso esto siguiente va directo a ti, a mi querido oyente.")
			queue_text("Que quisiste esa noche? Hasta donde querias llegar con tus acciones?")
			queue_text("Todavia no les estas contando la verdad?")
			queue_text("...")
			queue_text("Esta siguiente cancion llego al Top 3 de las listas Bullybord, escuchamos Crush My Head de RushMe.")
		if caso == 8:
			activo = true
			print("Starting state: State.READY")
			hide_textbox()
			label.add_theme_color_override("font_color", "ffffff")
			queue_text("La radio me estaba hablando a mi?")
			queue_text("...")
			queue_text("A que se refiere con que hice esa noche y las acciones? Si no recuerdo mal solo me escape y no mire hacia atras.")
			queue_text("Pero tenia una razon clara, no podia estar mas en esa casa, lo que sufria de parte de mi madre era inhumano.")
			queue_text("Me acuerdo cuando fallecio mi padre que no me llevaron al velorio, a la unica persona que me queria realmente en mi vida y no lo pude despedir.")
			queue_text("Encima tampoco me dijieron las causas, o si?")
			queue_text("Creo que si pero tengo todo muy difuso, no se habia quitado su propia vida? Realmente es algo muy doloroso de recordar.")
			queue_text("Pero estando en esta situacion lo necesito. Necesito saber todo lo que paso esa noche y sobre mi padre.")
		if caso == 10:
			activo = true
			print("Starting state: State.READY")
			hide_textbox()
			queue_text("Asi que habian sido ellos los culpables.")
		if caso == 11 && posicion.distance_to(rangoPosicion1) < 1:
			activo = true
			print("Starting state: State.READY")
			hide_textbox()
			queue_text("Todo tiene sentido, el bosque, la radio, el edificio, el telefono, la escopeta.")
			queue_text("Todo esta conectado, pero me habia olvidado todo. Como puede pasar esto? ")
			queue_text("No me merecia esta vida, por que? Por que todo lo que tocaba al final desaparecia? Por que mi madre y su novio me trataban asi?")
			queue_text("Por que para ser feliz me tuve que escapar de mi propia casa? El lugar donde uno se tiene que sentir bien y comodo era mi prision, por eso me escape.")
			queue_text("Me escape para no terminar como mi padre.")
			queue_text("Me escape...")
		if caso == 12:
			activo = true
			print("Starting state: State.READY")
			hide_textbox()
			label.add_theme_color_override("font_color", "c56231e5")
			queue_text("Ya vas entiendo como funciona esto no? Vas recordando poco a poco?")
			queue_text("Como poco a poco te fuiste hundiendo en tus miserias")
			queue_text("Como poco a poco fuiste tirando todo a la basura")
			queue_text("Y al final por tus decisiones te quedaste solo, en la oscuridad absoluta.")
			queue_text("Vacio absoluto.")
			queue_text("Pero yo se que te faltan algunas cosas mas por descubrir, algo que todavia no te acuerdas de aquella noche.")
			queue_text("Que hiciste con mama y su pareja? Te acordas?")
			queue_text("...")
			queue_text("Asi que te dedico esta siguiente cancion para que pienses mejor y termines con esto.")
		if caso == 13:
			activo = true
			print("Starting state: State.READY")
			hide_textbox()
			label.add_theme_color_override("font_color", "ffffff")
			queue_text("Asi que todavia me quedan cosas no?")
			queue_text("Que paso con mi madre?")
			queue_text("Es hora de volver...")
		if caso == 15:
			activo = true
			print("Starting state: State.READY")
			hide_textbox()
			queue_text("Entonces asi fue, ellos dos estan ahi en el bosque junto a la fogata")
			queue_text("Ya lo recuerdo...")
			queue_text("...")
			queue_text("Pero todavia hay algo que necesito ver")
		if caso == 16:
			activo = true
			print("Starting state: State.READY")
			hide_textbox()
			queue_text("Entonces fue asi, ya lo recuerdo...")
			
			
func queue_text(next_text):
	text_queue.push_back(next_text)

func hide_textbox():
	start_symbol.text = ""
	end_symbol.text = ""
	label.text = ""
	textbox_container.hide()
	
func show_textbox():
	start_symbol.text = "*"
	textbox_container.show()

func add_text(next_text):
	label.text = next_text
	show_textbox()
	
func display_text():
	var next_text = text_queue.pop_front()
	label.text = next_text
	label.visible_ratio = 0.0
	change_state(State.READING)
	show_textbox()
	
	while label.visible_characters <= len(next_text):
		if label.visible_characters == -1:
			break
		label.visible_characters += 1
		await get_tree().create_timer(0.02).timeout
		if label.visible_characters > len(next_text[2]) || label.visible_characters == -1:
			end_symbol.text = "v"
			current_state = State.FINISHED

func change_state(next_state):
	current_state = next_state
	match current_state:
		State.READY:
			print("Changing state to: State.READY")
		State.READING:
			print("Changing state to: State.READING")
		State.FINISHED:
			print("Changing state to: State.FINISHED")


func _on_static_body_3d_interacted(body):
	if caso == 16:
		seguir = true
	
	
func _on_audio_stream_player_3d_finished():
	if caso == 1:
		caso += 1
		activo = false
		seguir = true
	if caso == 7:
		activo = false
		seguir = true
	if caso == 12:
		activo = false
		seguir = true
	


func _on_texto_container_hidden():
	if activo:
		textoCaso += 1 
		
		if caso == 2 && textoCaso == 5:
			textoCaso = 0
			activo = false
			caso += 1
		if caso == 3 && textoCaso == 5:
			textoCaso = 0
			activo = false
			caso += 1
		if caso == 5 && textoCaso == 1:
			textoCaso = 0
			activo = false
			caso += 1
		if caso == 6 && textoCaso == 3:
			textoCaso = 0
			activo = false
			seguir = false
			caso += 1
		if caso == 7 && textoCaso == 7:
			textoCaso = 0
			activo = false
			caso += 1
		if caso == 8 && textoCaso == 8:
			textoCaso = 0
			activo = false
			caso += 1
			interactuableRadio = true
		if caso == 10 &&  textoCaso == 1:
			textoCaso = 0
			activo = false
			caso += 1
		if caso == 11 && textoCaso == 6:
			textoCaso = 0
			activo = false
			seguir = false
			caso += 1
		if caso == 12 && textoCaso == 9:
			textoCaso = 0
			activo = false
			caso += 1
		if caso == 13 && textoCaso == 3:
			textoCaso = 0
			activo = false
			caso += 1
			interactuableRadio = true
		if caso == 15 && textoCaso == 3:
			textoCaso = 0
			activo = false
			seguir = false
			caso += 1
		if caso == 16 && textoCaso == 1:
			textoCaso = 0
			activo = false
			seguir = false
			caso = 1


func _on_static_body_3d_2_interacted(body):
	if interactuableRadio:
		caso += 1
		interactuableRadio = false
