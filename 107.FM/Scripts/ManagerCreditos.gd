extends Node2D
@onready var animation_player = $CanvasFade/AnimationPlayer


# Called when the node enters the scene tree for the first time.
func _ready():
	$CanvasFade/AnimationPlayer.play("AnimacionABlanco")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_audio_stream_player_finished():
	$CanvasFade/AnimationPlayer.play("AnimacionANegro")

func _on_animation_player_animation_finished(anim_name):
	if anim_name == "AnimacionANegro":
		get_tree().change_scene_to_file("res://Escenas/Menu.tscn")
