extends AudioStreamPlayer
@onready var jugador = $"../Jugador"

var cambiarAudio
var activo

# Called when the node enters the scene tree for the first time.
func _ready():
	Audio1()



func _process(delta):
	var posicion = jugador.global_position

	var rangoPosicion1 = Vector3(3, -13.68584, 0)
	var rangoPosicion2 = Vector3(3, 0, -7)
	var rangoPosicion3 = Vector3(19.269, 0.067, 23.879)

	if posicion.distance_to(rangoPosicion1) < 1:  
		cambiarAudio = true

	if posicion.distance_to(rangoPosicion2) < 0.001:
		cambiarAudio = false
	
		
	if !cambiarAudio && !activo:
		Audio1()
		activo = true
	elif  cambiarAudio && activo:
		Audio2()
		activo = false

func Audio1():
	var audioStream: AudioStream = preload("res://Assets/Audio/SFX/SonidosBosqueLoop.mp3")
	self.set_stream(audioStream)
	self.set_volume_db(3.0)
	play()
	
func Audio2():
	var audioStream: AudioStream = preload("res://Assets/Audio/SFX/SonidosMazmorra.mp3")
	self.set_stream(audioStream)
	self.set_volume_db(3.0)
	play()
	
