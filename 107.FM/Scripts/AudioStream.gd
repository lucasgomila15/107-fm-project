extends AudioStreamPlayer3D
@onready var timer_radio = $"../TimerRadio"
@onready var radio = $".."

var audio = "Audio1"
var activo = false
var caso = 1
var textoCaso = 0
var seguir = false
var final = false

func _on_button_interacted(body):
	seguir = true
		
func _process(delta):
	if seguir && !activo:
		if caso == 1:
			audio1()
			activo = true
		if  caso == 2:
			audio2()
			activo = true
		if caso == 3:
			audioParar()
			audioCorte()
			activo = true
		if caso == 4:
			audio1()
			activo = true
		if caso == 5:
			audio4()
			activo = true
		if caso == 6:
			audioParar()
			audioCorte()
			activo = true
		if caso == 7:
			audio5()
			activo = true
		if caso == 8:
			audio6()
			activo = true
		if caso == 9:
			audioParar()
			audioCorte()
			activo = true

func _on_timer_timeout():
	caso += 1
	activo = false
	pass
	

func audioParar():
	stop()
	
func audioCorte():
	var audioStream: AudioStream = preload("res://Assets/Audio/SFX/radioAudio.mp3")
	self.set_stream(audioStream)
	self.set_volume_db(3.0)
	play()

func audio1():
	var audioStream: AudioStream = preload("res://Assets/Audio/2124(mixed).mp3")
	self.set_stream(audioStream)
	self.set_volume_db(3.0)
	play()

func audio2():
	var audioStream: AudioStream = preload("res://Assets/Audio/281223.mp3")
	self.set_stream(audioStream)
	self.set_volume_db(3.0)
	play()
	
func audio3():
	var audioStream: AudioStream = preload("res://Assets/Audio/31242.mp3")
	self.set_stream(audioStream)
	self.set_volume_db(3.0)
	play()
	
func audio4():
	var audioStream: AudioStream = preload("res://Assets/Audio/171242.mp3")
	self.set_stream(audioStream)
	self.set_volume_db(3.0)
	play()

func audio5():
	var audioStream: AudioStream = preload("res://Assets/Audio/2124_low.mp3")
	self.set_stream(audioStream)
	self.set_volume_db(3.0)
	play()

func audio6():
	var audioStream: AudioStream = preload("res://Assets/Audio/5224.mp3")
	self.set_stream(audioStream)
	self.set_volume_db(3.0)
	play()


func _on_static_body_3d_interacted(body):
	if !seguir:
		seguir = true


func _on_finished():
	if caso == 1:
		caso += 1
		activo = false
	if caso == 4:
		caso += 1
		activo = false
	if caso == 7:
		caso += 1
		activo = false

func _on_texto_container_hidden():
	if activo:
		textoCaso += 1 
		
		if caso == 2 && textoCaso == 5:
			self.stop()
			textoCaso = 0
			activo = false
			caso += 1
		
		if caso == 3 && textoCaso == 6:
			self.stop()
			textoCaso = 0
			activo = false
			seguir = false
			caso += 1
			
		if caso == 5 && textoCaso == 7: 
			self.stop()
			textoCaso = 0
			activo = false
			caso += 1
			
		if caso == 6 && textoCaso == 8:
			self.stop()
			textoCaso = 0
			activo = false
			seguir = false
			caso += 1
		
		if caso == 8 && textoCaso == 9:
			self.stop()
			textoCaso = 0
			activo = false
			caso += 1
			
		if caso == 9 && textoCaso == 3:
			self.stop()
			textoCaso = 0
			activo = false
			seguir = false
			final = true
			caso += 1

func _on_animation_player_animation_finished(anim_name):
	if anim_name == "AnimacionANegro":
		if final:
			radio.global_position =  Vector3(-20, 0.25, 23.972)
			radio.rotation_degrees = Vector3(0, 90, 0)
