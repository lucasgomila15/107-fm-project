extends CharacterBody3D
@onready var punto_camara = $PuntoCamara
@onready var colision_parado = $ColisionParado
@onready var colision_agachado = $ColisionAgachado
@onready var ray_cast_3d = $RayCast3D
@onready var timer = $Timer
@onready var audio_stream_player_3d_2 = $AudioStreamPlayer3D2

var velocidadActual = 4.0

const velocidadCaminar = 4.0
const velocidadCorrer = 8.0
const velocidadAgachar = 3.0
const saltoVelocidad = 4.5
const sensibilidadMouse = 0.3
var velocidadLerp = 10.0
var direction = Vector3.ZERO
var camaraAgachar = -0.5
var cambiarAudio 
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
var movimientoPosible = true

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	cambiarAudio = false
		

func _process(delta):
	var posicion = position

	var rangoPosicion1 = Vector3(3, -13.68584, 0)
	var rangoPosicion2 = Vector3(3, 0, -7)
	var rangoPosicion3 = Vector3(19.269, 0.067, 23.879)

	if posicion.distance_to(rangoPosicion1) < 1:  
		cambiarAudio = true

	if posicion.distance_to(rangoPosicion2) < 0.001:
		cambiarAudio = false
		
	if posicion.distance_to(rangoPosicion3) < 1:  
		cambiarAudio = false

func _input(event):
	if event is InputEventMouseMotion:
		rotate_y(deg_to_rad(-event.relative.x * sensibilidadMouse))
		punto_camara.rotate_x(deg_to_rad(-event.relative.y * sensibilidadMouse))
		punto_camara.rotation.x = clamp(punto_camara.rotation.x, deg_to_rad(-89), deg_to_rad(89))

func _physics_process(delta):
	
	if Input.is_action_pressed("Agacharse"):
		#velocidadActual = velocidadAgachar
		#punto_camara.position.y = lerp(punto_camara.position.y,1.8 + camaraAgachar, delta*velocidadLerp)
		#colision_parado.disabled = true
		#colision_agachado.disabled = false}
		pass
	elif !ray_cast_3d.is_colliding():
		#colision_agachado.disabled = true
		#colision_parado.disabled = false
		#punto_camara.position.y = lerp(punto_camara.position.y,1.8, delta*velocidadLerp)
		pass
		if Input.is_action_pressed("Correr"):
			#velocidadActual = velocidadCorrer
			pass
		else: 
			velocidadActual = velocidadCaminar
	
	if not is_on_floor():
		velocity.y -= gravity * delta

	if Input.is_action_just_pressed("Saltar") and is_on_floor():
		#velocity.y = saltoVelocidad
		pass
	
	if movimientoPosible:
		var input_dir = Input.get_vector("Izquierda", "Derecha", "Adelante", "Atras")
		direction = lerp(direction,(transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized(), delta * velocidadLerp)
	
		if direction:
			velocity.x = direction.x * velocidadActual
			velocity.z = direction.z * velocidadActual
		
		else:
			$AudioStreamPlayer3D.stop()
			velocity.x = move_toward(velocity.x, 0, velocidadActual)
			velocity.z = move_toward(velocity.z, 0, velocidadActual)
	
		if Input.is_action_pressed("Adelante") || Input.is_action_pressed("Atras") || Input.is_action_pressed("Izquierda") || Input.is_action_pressed("Derecha"):
			if cambiarAudio == false:
				audioPitch1()
			elif  cambiarAudio == true:
				audioPitch2()
		
		move_and_slide()
	
func audioPitch1():
	if $Timer.time_left <= 0:
			$AudioStreamPlayer3D.pitch_scale = randf_range(0.8, 1.2)
			$AudioStreamPlayer3D.play()
			$Timer.start(0.6)

func audioPitch2():
	if $Timer.time_left <= 0:
			$AudioStreamPlayer3D2.pitch_scale = randf_range(1.5, 2)
			$AudioStreamPlayer3D2.play()
			$Timer.start(0.8)
	


func _on_animation_player_animation_finished(anim_name):
	if anim_name == "AnimacionABlanco":
		movimientoPosible = true


func _on_animation_player_animation_started(anim_name):
	if anim_name == "AnimacionANegro":
		movimientoPosible = false
