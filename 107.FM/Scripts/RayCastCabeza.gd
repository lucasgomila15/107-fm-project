extends RayCast3D

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if is_colliding():
		var detected = get_collider()
		
		if detected is Interactable:
			if Input.is_action_just_pressed("Interactuar"):
				detected.interactuar(owner)
