class_name Interactable
extends StaticBody3D

signal interacted(body)

@export var prompt_message = "Interact"
@export var prompt_action = "interact"

func interactuar(body):
	emit_signal("interacted", body)
