extends Node3D
@onready var animation_player = $CanvasFade/AnimationPlayer
@onready var empezar = $VBoxContainer/Empezar

func _ready():
	$CanvasFade/AnimationPlayer.play("AnimacionABlanco")
	empezar.grab_focus()

func _process(delta):
	pass


func _on_empezar_pressed():
	$CanvasFade/AnimationPlayer.play("AnimacionANegro")


func _on_animation_player_animation_finished(anim_name):
	if anim_name == "AnimacionANegro":
		get_tree().change_scene_to_file("res://Escenas/Bosque.tscn")


func _on_salir_pressed():
	get_tree().quit()


func _on_controles_pressed():
	pass # Replace with function body.
