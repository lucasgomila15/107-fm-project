extends Node3D
@onready var audio_stream_player = $AudioStreamPlayer
@onready var animation_player = $"../CanvasFade/AnimationPlayer"
@onready var jugador = $"../Jugador"
@onready var punto_spawn = $"../Dungeon/PuntoSpawn"
@onready var telefono = $"../Dungeon/Telefono"
@onready var escopeta = $"../Dungeon/Escopeta"
@onready var pala = $"../Dungeon/Pala"

var dungeon = false
var textoCaso = 0
var activo = false
var casoTexto = 0
var caso = 1
var salir = false

# Called when the node enters the scene tree for the first time.
func _ready():
	$"../CanvasFade/AnimationPlayer".play("AnimacionABlanco")
	jugador.global_position = Vector3(0, 0, 0)
	jugador.rotation_degrees = Vector3(0, 0, 0)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if textoCaso == 54:
		$"../CanvasFade/AnimationPlayer".play("AnimacionANegro")
	
	if Input.is_action_pressed("ESC"):
		$"../CanvasFade/AnimationPlayer".play("AnimacionANegro")
		salir = true
		

func _on_texto_container_hidden():
	textoCaso += 1 
	if textoCaso == 11:
		$"../CanvasFade/AnimationPlayer".play("AnimacionANegro")
		telefono.show()
	if textoCaso == 12:
		$"../CanvasFade/AnimationPlayer".play("AnimacionANegro")
	if textoCaso == 30:
		$"../CanvasFade/AnimationPlayer".play("AnimacionANegro")
		escopeta.show()
	if textoCaso == 31:
		$"../CanvasFade/AnimationPlayer".play("AnimacionANegro")
	if textoCaso == 49:
		$"../CanvasFade/AnimationPlayer".play("AnimacionANegro")
		pala.show()
	if textoCaso == 53:
		$"../CanvasFade/AnimationPlayer".play("AnimacionANegro")
		

func ir_dungeon():
	jugador.global_position = Vector3(3, -13.7, 0)
	jugador.rotation_degrees = Vector3(0, 0, 0)
	
func ir_bosque():
	jugador.global_position = Vector3(3, 0, -7)	
	jugador.rotation_degrees = Vector3(0, 80, 0)
	
func ir_final():
	jugador.global_position = Vector3(19.269, 0.067, 23.879)	
	jugador.rotation_degrees = Vector3(0, 80, 0)

func _on_animation_player_animation_finished(anim_name):
	if anim_name == "AnimacionANegro":
		if salir:
			get_tree().change_scene_to_file("res://Escenas/Menu.tscn")
		
		if textoCaso == 11:
			ir_dungeon()
			$"../CanvasFade/AnimationPlayer".play("AnimacionABlanco")
		if textoCaso == 12:
			ir_bosque()
			telefono.hide()
			$"../CanvasFade/AnimationPlayer".play("AnimacionABlanco")
		if textoCaso == 30:
			ir_dungeon()
			$"../CanvasFade/AnimationPlayer".play("AnimacionABlanco")
		if textoCaso == 31:
			ir_bosque()
			escopeta.hide()
			$"../CanvasFade/AnimationPlayer".play("AnimacionABlanco")
		if textoCaso == 49:
			ir_dungeon()
			$"../CanvasFade/AnimationPlayer".play("AnimacionABlanco")
		if textoCaso == 53:
			ir_final()
			pala.hide()
			$"../CanvasFade/AnimationPlayer".play("AnimacionABlanco")
		if textoCaso == 54:
			get_tree().change_scene_to_file("res://Escenas/Creditos.tscn")


func _on_static_body_3d_2_interacted(body):
	$AudioStreamPlayer.play()


func _on_static_body_3d_interacted(body):
	$AudioStreamPlayer.play()
